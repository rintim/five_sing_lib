defmodule FiveSing do
  @moduledoc """
  Documentation for `FiveSing`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> FiveSing.hello()
      :world

  """
  def hello do
    :world
  end
end
