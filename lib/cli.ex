defmodule FiveSing.CLI do
  def main([url | _]) do
    {:ok, dir_path} = Temp.mkdir "five_sing"
    re = ~r/http:\/\/5sing.kugou.com\/(?<type>.+?)\/(?<id>.+?).html/
    %{"id" => id, "type" => type} = Regex.named_captures(re, url)
    data = [
      from: :web,
      version: "6.7.72",
      songtype: type,
      songid: id
    ]

    Esolicit.get_json("http://service.5sing.kugou.com/song/getSongUrl?jsoncallback=?", data, jquery: true)
    |> Access.get("data")
    |> Access.get("hqurl")
    |> Download.Server.stream()
    |> Stream.into(
      File.stream!(
        Path.join(
          [dir_path, "#{Access.get(json, "data") |> Access.get("songName")}.#{json |> Access.get("data") |> Access.get("hqext")}"]
        )
      )
    )
    |> Stream.run()

    System.cmd("mv", [Path.join(
      [dir_path, "#{json |> Access.get("data") |> Access.get("songName")}.#{json |> Access.get("data") |> Access.get("hqext")}"]
    ), "./"])
    IO.puts("Download Done.")
    System.halt(0)
  end
end
